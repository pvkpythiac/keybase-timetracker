#!/usr/bin/env python3
import asyncio
import datetime
import inspect
import json
import logging
import os
import re
import subprocess # noqa
import sys
from copy import deepcopy
from csv import DictReader, DictWriter
from os import getpid
from pathlib import Path
from shutil import rmtree
from typing import List

import pykeybasebot
from git import Actor, GitCommandError, Repo
from pykeybasebot.types import chat1
from setproctitle import setproctitle

from decorators import IsChatMsg as is_chat_msg
from decorators import IsSystemMsg as is_system_msg
from exceptions import ActionError
from utils import force_async


# TODO: Split this on little regexes
FIX_SENTENCE_POSIBILITIES = [
    r'^(?P<fix_command>brb|hi|back|hello|bye)\s(?P<fix_hour>[0-1]?[0-9]|2[0-3]):(?P<fix_minute>[0-5][0-9])(?:(?:\:)?(?P<fix_second>[0-5][0-9])?)(?P<fix_sentence>.*)', # noqa
    r'^(?P<fix_command>brb|hi|back|hello|bye)\s(?P<fix_year>\d{4})\-(?P<fix_month>0?[1-9]|1[012])\-(?P<fix_day>0?[1-9]|[12][0-9]|3[01])\s(?P<fix_hour>[0-1]?[0-9]|2[0-3]):(?P<fix_minute>[0-5][0-9])(?:(?:\:)?(?P<fix_second>[0-5][0-9])?)(?P<fix_sentence>.*)' # noqa
    r'^(?P<fix_command>brb|hi|back|hello|bye)\s(?P<fix_hour>[0-1]?[0-9]|2[0-3]):(?P<fix_minute>[0-5][0-9])(?:(?:\:)?(?P<fix_second>[0-5][0-9])?)\s(?P<fix_day>0?[1-9]|[12][0-9]|3[01])\-(?P<fix_month>0?[1-9]|1[012])\-(?P<fix_year>\d{4})(?P<fix_sentence>.*)' # noqa
]

TEAM_NAME_REGEXP = re.compile(r'^[a-z]+(_[a-z0-9]+)*[a-z0-0]$')

if "win32" in sys.platform:
    # Windows specific event-loop policy
    asyncio.set_event_loop_policy(
        asyncio.WindowsProactorEventLoopPolicy()  # type: ignore
    )

logging.basicConfig(level=logging.ERROR, stream=sys.stdout)


class KBot:

    log = logging.getLogger(__name__)
    messages = {}

    def __init__(self, botname, paperkey):
        self.botname = botname
        self.paperkey = paperkey
        self._commands = []

    def run(self):
        setproctitle('kbot')
        self._bot = pykeybasebot.Bot(
            username=self.botname, paperkey=self.paperkey, handler=self.command_handler
        )
        asyncio.run(self._bot.start({}))

    def add_command(self, trigger_func, command_func):
        async_command = command_func
        if not inspect.iscoroutinefunction(command_func):
            async_command = force_async(command_func)
        self._commands.append(
            {"trigger_func": trigger_func, "command_func": async_command}
        )

    def store_event(self, event: pykeybasebot.kbevent.KbEvent):
        team_name = event.msg.channel.name
        topic_name = event.msg.channel.topic_name
        self.messages.setdefault(f'{team_name}#{topic_name}', [])
        self.messages[f'{team_name}#{topic_name}'].append(event)

    def say(self, team_name: str, message: str, topic_name: str = None):
        # make a new loop to run the async bot.chat.send method in what is currently
        # a synchronous context
        # loop = asyncio.new_event_loop()
        # asyncio.set_event_loop(loop)
        # kwargs = {'members_type': 'team'} if ',' not in team else {}
        # channel = pykeybasebot.types.chat1.ChatChannel(
        #     name=team,
        #     topic_type='chat',
        #     topic_name=topic_name,
        #     **kwargs
        # )
        # result = loop.run_until_complete(self._bot.chat.send(channel, message))
        result = await self._bot.chat.send(team_name, message)
        self.store_event(result)
        return result

    async def reply(self, event, message: str):
        # make a new loop to run the async bot.chat.send method in what is currently
        # a synchronous context
        result = await self._bot.chat.send(event.msg.channel, message)
        # loop = asyncio.new_event_loop()
        # asyncio.set_event_loop(loop)
        # result = loop.run_until_complete()
        self.store_event(result)
        return result

    def delete(self, channel: chat1.ChatChannel, message_id: str):
        # make a new loop to run the async bot.chat.excecute method in what is currently
        # a synchronous context
        delete_order = {
            "method": "delete",
            "params": {
                "options": {"channel": channel.to_dict(), "message_id": message_id}
            },
        }
        # loop = asyncio.new_event_loop()
        # asyncio.set_event_loop(loop)
        # result = loop.run_until_complete(self._bot.chat.execute(delete_order))
        result = await self._bot.chat.execute(delete_order)
        return result

    def leave(self, channel: chat1.ChatChannel, permanent: bool = False):
        leave_order = {
            "method": "leave-team",
            "params": {
                "options": {
                    "team": channel.name,
                    "permanent": permanent
                }
            }
        }
        try:
            command = subprocess.run(['keybase', 'team', 'api'],  # noqa
                                     input=json.dumps(leave_order),
                                     stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
        except BaseException as excp:
            self.log.exception(excp, extra=channel.to_dict())

        if command.returncode:
            subprocess.CalledProcessError(command.return_code, stderr=command.stderr)
            self.log.exception

    async def command_handler(self, bot, event: pykeybasebot.kbevent.KbEvent):
        for possibility in self._commands:
            if possibility["trigger_func"](event):
                return await possibility["command_func"](event)


class CustomBot(KBot):

    repos_path = Path.cwd().joinpath('repos').resolve()
    _available_commands = ['!bye', '!hi', '!hello', '!brb', '!back']
    _extra_commands = ['!help', '!oops']

    async def read_groups(self, group_name: str, revision: int = None) -> List[str]:
        namespace = 'config'
        team_name = f'{self.botname},{self.botname}'
        res = await self._bot.kvstore.get(namespace, group_name, team=team_name, revision=None)
        if not res.entry_value:
            return []

        try:
            json.loads(res.entry_value)
        except BaseException as excp:
            self.log.exception(excp, extra={
                'entry_value': res.entry_value,
                'group_name': group_name,
                'tean_name': team_name,
                'namespace': namespace
            })
            return []

    async def groupadd(self, group_name: str, force: bool = False) -> bool:
        if not isinstance(group_name, str):
            excp = TypeError(f'{group_name} is not a valid string')
            self.log.exception(excp, extra={
                'group_name': group_name,
                'force': force,
                'variable_type': type(group_name)
            })
            return False

        namespace = 'config'
        team_name = f'{self.botname},{self.botname}'
        groups = self.read_groups(group_name)
        groups.append(group_name)
        await self._bot.kvstore.put(namespace, group_name, team=team_name)

    def get_used_path(self, team_name: str):
        first_letter, second_letter, third_letter = team_name[0], team_name[1], team_name[2]
        return self.repos_path.joinpath(first_letter).joinpath(second_letter).joinpath(third_letter) \
                              .joinpath(team_name[3:])

    def find_fix_sentences(self, sentence: str):
        for item in FIX_SENTENCE_POSIBILITIES:
            match = re.compile(item).match(sentence)
            if match:
                return match

    def setup_notifications(self, repo_name: str, team_name: str, topic_name: str = None):
        command = ['keybase', 'git', 'settings']

        enabled = False
        if topic_name:
            enabled = True
            command.extend(['--channel', topic_name])
        else:
            command.append('--disable-chat')

        command.append(repo_name)

        proc = subprocess.Popen(command, stderr=subprocess.PIPE, stdout=subprocess.PIPE, text='utf-8')  # noqa
        stdout, stderr = proc.communicate()
        return_code = proc.poll()

        if not stderr:
            # self.reply(event, )
            # self.store_event(event.msg.channel, event.msg.topic_name, result)
            disabled_msg = f'Notifications for {team_name}/{repo_name} enabled on #{topic_name}'
            enabled_msg = f'Notifications for {team_name}/{repo_name} disabled on #{topic_name}'
            return True, enabled_msg if enabled else disabled_msg

        if 'denied' not in stderr:
            False, 'Unexpected error happening while modifying notification settings, please check logs'
            self.log.exception(subprocess.CalledProcessError(returncode=return_code, stdout=stdout, stderr=stderr))
        return False, (f'I cannot setup notifications {team_name}/{repo_name} #{topic_name} due to missing permissions.'
                       'Please check your team settings.')

    def get_existing_repo_url(self, repo_name, username=None, team_name=None):
        if not username or not team_name:
            raise ValueError('Either username or team_name must be provided.')

        if not team_name:
            return f'keybase://private/{username}/{repo_name}'
        return f'keybase://team/{team_name}/{repo_name}'

    def create_repo(self, repo_name, team_name=None, force=False):

        command = ['keybase', 'git', 'create']
        repo_full_name = repo_name
        repo_url = None
        already_exists = False
        force_str = ''
        msg_str = ''

        if team_name:
            command.extend(['--team', team_name])
            repo_full_name = f'company_name/{repo_name}'
        command.append(repo_name)

        proc = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text='utf-8')  # noqa
        stdout, stderr, = proc.communicate()

        if stderr:
            already_exists = f'"{repo_name}" already exists' in stderr

        if not already_exists and stdout:
            return None, stdout.split('\n')[1][12:]
        proc.terminate()

        if force and already_exists:
            force_str = 'But we will use it anyway.' if force else ''
            repo_url = self.get_existing_repo_url(repo_name, team_name=team_name)
            return repo_url, force_str

        msg_str = f' The repo {repo_full_name} already exists. {force_str}'.strip()
        msg_str = msg_str if already_exists else None

        return repo_url, msg_str

    def check_valid_team_name(self, team_name):
        if len(team_name) < 3 or len(team_name) > 16:
            return False, 'Team names must be between, 3 and 16 characters long.'

        matches = TEAM_NAME_REGEXP.match(team_name)
        if not matches:
            reason = "Keybase team names must be letters (a-z), numbers, and underscores." + \
                "Also, they can't start with underscores or use double underscores, to avoid confusion."
            return False, reason
        return True, ''

    def create_channel(self, team_name, channel_name, topic_type=None):

        command = ['keybase', 'chat', 'create-channel']
        topic_type = topic_type or 'chat'

        if topic_type:
            command.extend(['--topic_type', topic_type])

        command.extend([team_name, channel_name])

        is_valid, reason = self.check_valid_team_name(team_name)
        if not is_valid:
            return False, reason

        proc = subprocess.Popen(command, stderr=subprocess.PIPE, stdout=subprocess.PIPE)  # noqa
        stdout, stderr = proc.communicate()

        proc.terminate()
        if stderr:
            return False, stderr.decode()[60:]
        else:
            return True, f'We have created a channel called {channel_name} in {team_name} group.'

        # self.reply(event, )
        # return channel_name
        # return channel_name

    def clone_repo(self, repo_url, username=None, team_name=None, force=False):
        if not username or not team_name:
            raise ValueError('No username or team_name provided')

        # TODO CHECK GET USED PATH
        # MUST SET TO PRIVATE IF USERNAME ELSE TEAM FOLDER
        repo_path = self.get_used_path(username or team_name)
        if force and repo_path.exists():
            rmtree(repo_path)
        repo_path.mkdir(exist_ok=True, parents=True)
        Repo.clone_from(repo_url, repo_path)

    def register_company(self, team_name, force=False):
        repo_name = 'worklog_hours'
        repo_url, reason = self.create_repo(repo_name, team_name=team_name, force=force)

        if not repo_url:
            return {'status': 'error', 'messages': [reason]}

        self.clone_repo(repo_url, team_name, force=force)

        messages = []
        mandatory_channels = ['WorkHoursPushLog', 'general']
        for item in mandatory_channels:
            is_created, message = self.create_channel(team_name, item)
            messages.append(message)

        messages.append(self.setup_notifications(repo_name, team_name, topic_name='WorkHoursPushLog'))

        status = 'warning' if len(messages) != len(mandatory_channels) else 'ok'
        return {'status': status, 'messages': messages}

    def is_registered_company(self, company_name):
        return Path(self.get_used_path(company_name)).exists()

    def do_commit(self, event, repo_file):
        company_name = event.msg.channel.name
        username = event.msg.sender.username
        device_id = event.msg.sender.device_id
        device_name = event.msg.sender.device_name
        commit = f'Signed by {username} on {device_name} ({device_id})'
        repo_path = self.get_used_path(company_name)
        repo = Repo(repo_path)
        try:
            repo.remote('origin').pull('master')
        except GitCommandError as err:
            self.log.warning(str(err.stderr))

        if not repo.untracked_files and not repo.is_dirty():
            return

        repo.index.add([str(repo_file)])
        repo.index.commit(commit, author=Actor(username, f'{username}@{device_name}'))
        self.log.info(commit)
        try:
            repo.remote('origin').push('master')
        except GitCommandError as excp:
            self.log.exception(excp)

    def read_timestamp_file(self, repo_file):
        if not repo_file.exists():
            return []
        rows = []
        with open(repo_file, 'r') as csv_file:
            rows = [
                row for row in DictReader(csv_file)
            ]
            csv_file.close()
            return rows

    def get_repo_file(self, event, timestamp):
        used_date = datetime.datetime.fromtimestamp(int(timestamp))
        username = event.msg.sender.username
        month = str(used_date.month).zfill(2)
        return self.get_used_path(event.msg.channel.name).joinpath(username).joinpath(f'{used_date.year}{month}.csv')

    def get_stored_timestamps(self, event, used_date):
        repo_file = self.get_repo_file(event, used_date.timestamp())
        return self.read_timestamp_file(repo_file)

    def append_action(self, event, repo_file, row_dict):
        create_header = not repo_file.exists()
        repo_file.parent.mkdir(exist_ok=True)

        with open(repo_file, 'a') as csv_file:
            field_names = ['timestamp', 'action', 'datetime', 'message', 'device_id', 'device_name']
            writer = DictWriter(csv_file, field_names, delimiter=',')
            if create_header:
                writer.writeheader()
            writer.writerow(row_dict)
            csv_file.close()

    def create_action(self, event, action, message=None, timestamp=None):

        if not action:
            raise ActionError('No action provided')

        timestamp = int(timestamp or event.msg.sent_at)
        repo_file = self.get_repo_file(event, timestamp)

        message = message or ''
        self.append_action(event, repo_file, {
            'timestamp': timestamp,
            'action': action,
            'message': message,
            'datetime': datetime.datetime.fromtimestamp(timestamp).isoformat(),
            'device_id': event.msg.sender.device_id,
            'device_name': event.msg.sender.device_name
        })
        self.do_commit(event, repo_file)

    def get_datetime_and_action(self, sentence, selected_datetime):
        def try_group(matches, group):
            try:
                return int(matches.group(group))
            except BaseException:
                return None

        matches = self.find_fix_sentences(sentence)
        if not matches:
            return None, None

        command_actions = {
            'brb': 'BREAK',
            'bye': 'LOGOUT',
            'back': 'BACK',
            'hi': 'LOGIN',
            'hello': 'LOGIN'
        }

        fix_command = matches.group('fix_command')
        fix_year = try_group(matches, 'fix_year')
        fix_month = try_group(matches, 'fix_month')
        fix_day = try_group(matches, 'fix_day')
        fix_hour = try_group(matches, 'fix_hour')
        fix_minutes = try_group(matches, 'fix_minute')
        fix_seconds = try_group(matches, 'fix_second') or 0

        if fix_year:
            fixed_datetime = datetime.datetime(fix_year, fix_month, fix_day, fix_hour, fix_minutes, fix_seconds, 0)
        else:
            fixed_datetime = datetime.datetime(selected_datetime.year, selected_datetime.month,
                                               selected_datetime.day, fix_hour, fix_minutes,
                                               fix_seconds, 0)

        return fixed_datetime, command_actions[fix_command],

    def can_do_action(self, event: pykeybasebot.kbevent.KbEvent, action: str, timestamp: int = None):

        timestamp = timestamp or event.msg.sent_at
        selected_datetime = datetime.datetime.fromtimestamp(timestamp)
        previous_month_date = deepcopy(selected_datetime).replace(day=1) - datetime.timedelta(days=1)

        last_month_stamps = self.get_stored_timestamps(event, previous_month_date)
        current_month_stamps = self.get_stored_timestamps(event, selected_datetime)
        stored_timestamps = deepcopy(last_month_stamps)
        stored_timestamps.extend(current_month_stamps)

        username = event.msg.sender.username
        used_date = None

        actions = {
            'LOGIN': 'LOGOUT',
            'BREAK': 'BACK',
            'LOGOUT': 'LOGIN',
            'BACK': 'BREAK'
        }

        last_action = None
        last_datetime = None
        last_timestamp = None
        if stored_timestamps:
            last_timestamp = stored_timestamps[-1]
            last_datetime = datetime.datetime.fromtimestamp(int(last_timestamp['timestamp']))
            last_action = last_timestamp['action']

        if stored_timestamps and last_datetime and last_datetime > selected_datetime:
            return False, f"@{username} Spacetime manipulation has been detected, incident will be reported."

        elif stored_timestamps and selected_datetime > datetime.datetime.utcnow():
            return False, f"@{username} Spacetime manipulation has been detected, incident will be reported."

        elif stored_timestamps and last_action == action:
            last_time = last_datetime if timestamp.date() == used_date.date() else timestamp.strftime('%Y-%m-%d %H:%M')
            return False, (f"Ey, @{username} you can\'t do '{action}', two times in a row, you already did that on "
                           f"{last_time}, please do {actions[action]} first or fixit by !oops.")

        elif action == 'LOGIN':
            if not stored_timestamps or last_action == 'LOGOUT':
                return True, f"Welcome @{username}!"
            return False, f"@{username} you need to logout first."

        elif action == 'LOGOUT':
            if not stored_timestamps:
                return False, f"@{username} you need to login first."
            elif last_action in ['BACK', 'LOGIN']:
                return True, f"Good bye @{username}."
            return False, f"You should go !back first @{username}."

        elif action == 'BACK':
            if stored_timestamps and last_action == 'BREAK':
                return True, f"Welcome back @{username}"
            return False, f"So... @{username} back from where exactly?"

        elif action == 'BREAK':
            if last_action in ['LOGIN', 'BACK']:
                return True, f"NP @{username}."

            elif last_action == 'BREAK':
                return True, f"You are already in a break @{username}."

            return False, f"@{username} you need to login first."

        elif action == 'OOPS':
            sentence = event.msg.content.text.body.split('!oops ')[1]
            try:
                fixed_datetime, fixed_action = self.get_datetime_and_action(sentence, selected_datetime)
            except ValueError:
                return False, f'Please, @{username}, your time string is not valid'
            if not fixed_action:
                return False, f'Please, @{username}, check your syntax.'

            can_do_action, reason = self.can_do_action(event,
                                                       fixed_action,
                                                       timestamp=int(fixed_datetime.timestamp()))
            if not can_do_action:
                return can_do_action, reason
            return True, f'@{username} Time travel succesfully'

        self.log.exception(ActionError('Unexpected combination of actions'), extra={
            'last_action': last_action,
            'action': action,
            'username': username,
            'company': event.msg.channel.name,
            'used_date': used_date
        })

        return False, 'Not implemented, please contact developers.'

    @is_system_msg
    def is_subteam_invite(self, event: pykeybasebot.kbevent.KbEvent):
        return '.' in event.msg.channel.name and event.msg.channel.members_type == 'team'

    @is_system_msg
    def is_team_invite(self, event: pykeybasebot.kbevent.KbEvent):
        return '.' not in event.msg.channel.name and event.msg.channel.members_type == 'team'

    @is_chat_msg
    def is_a_hi(self, event: pykeybasebot.kbevent.KbEvent):
        self.store_event(event)
        return event.msg.content.text.body.startswith(('!hi', '!hello'))

    @is_chat_msg
    def is_a_brb(self, event: pykeybasebot.kbevent.KbEvent):
        return event.msg.content.text.body.startswith(('!brb'))

    @is_chat_msg
    def is_a_back(self, event: pykeybasebot.kbevent.KbEvent):
        return event.msg.content.text.body.startswith(('!back'))

    @is_chat_msg
    def is_a_bye(self, event: pykeybasebot.kbevent.KbEvent):
        return event.msg.content.text.body.startswith(('!bye'))

    @is_chat_msg
    def is_a_oops(self, event: pykeybasebot.kbevent.KbEvent):
        return event.msg.content.text.body.startswith(('!oops'))

    @is_chat_msg
    def is_a_register(self, event: pykeybasebot.kbevent.KbEvent):
        return event.msg.content.text.body.startswith(('!register'))

    @is_chat_msg
    def is_a_exit(self, event: pykeybasebot.kbevent.KbEvent):
        return event.msg.content.text.body.startswith(('!exit'))

    async def leave_team(self, event: pykeybasebot.kbevent.KbEvent):
        _teams = await self.read_groups('teams')
        if event.msg.channel not in _teams:
            self.say(f'{self.botname},{event.msg.sender.username}', 'Due to data protection policy enforcement.')
            self.say(f'{self.botname},{event.msg.sender.username}', 'Please, read more at: XXXXX')
        return self.leave(event.msg.channel)

    async def do_welcome(self, event: pykeybasebot.kbevent.KbEvent):
        can_do, reason = self.can_do_action(event, 'LOGIN')
        # return await self.reply(event, 'lol')
        if not can_do:
            return self.reply(event, reason)
        return
        self.reply(event, reason)
        self.create_action(event, 'LOGIN')
        self.log.info('We should now do welcome')

    def do_break(self, event: pykeybasebot.kbevent.KbEvent):
        can_do, reason = self.can_do_action(event, 'BREAK')
        if not can_do:
            return self.reply(event, reason)
        self.create_action(event, 'BREAK')
        self.reply(event, reason)
        self.log.info('we should do now LOGIN')

    def do_back(self, event: pykeybasebot.kbevent.KbEvent):
        can_do, reason = self.can_do_action(event, 'BACK')
        if not can_do:
            return self.reply(event, reason)
        self.create_action(event, 'BACK')
        self.reply(event, reason)
        self.log.info('we should do now BACK')

    def do_fix(self, event: pykeybasebot.kbevent.KbEvent):
        can_do, reason = self.can_do_action(event, 'OOPS')
        if not can_do:
            return self.reply(event, reason)
        sentence = event.msg.content.text.body.split('!oops ')[1]
        matches = self.find_fix_sentences(sentence)
        message = matches.group('fix_sentence')
        selected_datetime = datetime.datetime.fromtimestamp(int(event.msg.sent_at))
        fixed_datetime, fixed_action = self.get_datetime_and_action(sentence, selected_datetime)
        username = event.msg.sender.username

        try:
            fixed_datetime, fixed_action = self.get_datetime_and_action(sentence, selected_datetime)
        except ValueError:
            return self.reply(f'Please, @{username}, your time string is not valid')

        self.create_action(event, fixed_action, message=message, timestamp=fixed_datetime.timestamp())
        self.reply(event, reason)

    def do_bye(self, event: pykeybasebot.kbevent.KbEvent):
        can_do, reason = self.can_do_action(event, 'LOGOUT')
        if not can_do:
            return self.reply(event, reason)
        self.create_action(event, 'LOGOUT')
        self.reply(event, reason)
        self.log.info('We should do now BYE')

    def do_register(self, event: pykeybasebot.kbevent.KbEvent):
        company_name = event.msg.content.text.body.split('!register')[1].strip()
        company_split = company_name.split(' ')
        company_name = company_split[0]
        force = True if len(company_split) >= 2 and company_split[1] == 'force' else False
        if not company_name:
            self.reply(event, f'@{event.msg.sender.username} no company name has been received.')
            return

        if self.is_registered_company(company_name) and not force:
            return self.reply(event, f'{company_name} has already been registed')

        self.register_company(event, company_name, force=force)

    def do_kill(self, event: pykeybasebot.kbevent.KbEvent):
        # check_existent_employee(self, event)
        # self.reply(event, f"Good bye cruel world")

        for item in self.messages:
            msg_id = getattr(item, 'message', 'id')
            msg_id = getattr(item, 'msg', 'id') if not msg_id else msg_id
            try:
                self.delete(event.channel, msg_id)
            except BaseException as excp:
                self.log.exception(excp, extra=item.to_dict())
        subprocess.run(['/usr/bin/kill', '-15', str(getpid())])  # noqa

    def add_default_commands(self):
        self.add_command(self.is_team_invite, self.leave_team)
        self.add_command(self.is_a_hi, self.do_welcome)
        self.add_command(self.is_a_brb, self.do_break)
        self.add_command(self.is_a_oops, self.do_fix)
        self.add_command(self.is_a_bye, self.do_bye)
        self.add_command(self.is_a_back, self.do_back)
        self.add_command(self.is_a_exit, self.do_kill)
        self.add_command(self.is_a_register, self.do_register)


pingbot = CustomBot(
    botname=os.environ.get('BOTNAME'),
    paperkey=os.environ.get('PAPERKEY'),
    channels=[],
)


pingbot.add_default_commands()
pingbot.run()
